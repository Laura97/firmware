/*
 * AVR Weather Station.c
 *
 * Created: 24/08/2016 3:01:07 PM
 * Author : Laura Robson
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>

/* Declare a constant that controls the LED's on the seven segment display */
uint8_t seven_seg_data[12] = {63, 6, 91, 79, 102, 109, 125, 7, 127, 111, 0, 64};

/* Declare functions defined later */
void output_temp(int8_t temperature);
void output_wind(float wind);
int8_t get_temp(void);
float get_wind(void);
void define_direction(void);
void serial_out(float output or int8_t output);
int main(void) {
	define_direction();
	while(1) { //going to be while not in standby
		int8_t temperature = get_temp();
		output_temp(temperature);
		serial_out(temperature);
		//wait time
		float wind = get_wind();
		output_wind(wind);
		serial_out(wind);
	}
}  

void output_temp(int8_t temperature) {
	uint8_t digit1 = 10;
	uint8_t digit2 = 10;
	uint8_t digit3 = 10;
	if (temperature < 10 && temperature >= 0) {
		digit3 = temperature;
	} else if (temperature >= 10) {
		digit2 = temperature / 10;
		digit1 = temperature % 10;
	} else if (temperature < 0 && temperature > -10) {
		digit1 = 11;
		digit3 = (temperature * -1);
	} else {
		digit1 = 11;
		digit2 = temperature / -10;
		digit3 = (temperature % 10) * -1;
	}
	//digits are left to right so for 213 2 = digit1 and 3 = digit3
	PORTD = 0x03;
	PORTB = (seven_seg_data[digit1]);
	PORTD = 0x02;
	PORTB = (seven_seg_data[digit2]);
	PORTD = 0x01;
	PORTB = (seven_seg_data[digit3]);
}

void output_wind(float wind) {
	int digit1 = 10;
	int digit2 = 0;
	int digit3 = 10;
	if (wind < 1) {
		digit3 = wind * 10;
	} else if (wind < 10 && wind >= 1) {
		digit2 = wind / 1;
		digit3 = (wind - digit2) * 10;
	} else {
		digit1 = wind / 10;
		int temp = wind / 1;
		digit2 = temp % 10;
		digit3 = (wind - ((digit1 * 10) + digit2)) * 10;
	}
	PORTD = 0x03;
	PORTB = (seven_seg_data[digit1]);
	PORTD = 0x02;
	PORTB = (seven_seg_data[digit2] + 128);
	PORTD = 0x01;
	PORTB = (seven_seg_data[digit3]);
}

void define_direction(void) {
	DDRB = 0xFF;
	DDRD = 0xFF;
}

int8_t get_temp(void) {
	ADMUX = 0x01; //set pin to convert from 
    ADCSRA |= (1<<ADSC);
    while (ADCSRA & (1<<ADSC));
		//empty loop
    int8_t temp = ADC;
	return 60 * temp - 10;
}

float get_wind(void) {
	ADMUX = 0x02; //set pin to convert from
	ADCSRA |= (1<<ADSC);
	while (ADCSRA & (1<<ADSC));
		//empty loop
	float temp = ADC;
	return 20 * temp;
}

void serial_out(float output or int8_t output) {
	//send over serial port
}
